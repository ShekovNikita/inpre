package com.sheniv.domain.model

class Category(
    val nameCategory: String,
    val image: String
)