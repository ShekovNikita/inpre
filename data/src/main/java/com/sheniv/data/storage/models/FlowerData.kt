package com.sheniv.data.storage.models

data class FlowerData(
    //val about: String,
    val articul: String,
    val category: String,
    val cost: String,
    val have: String,
    val hit: String,
    var amount: Int,
    val name: String,
    val img_source: List<String>
)