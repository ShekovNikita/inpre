package com.sheniv.inpre.di

import com.sheniv.inpre.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<MainFragmentViewModel> {
        MainFragmentViewModel(
            getAllFlowersFromDataUseCase = get(),
            postAmountValueNullUseCase = get(),
            changeFlowerInDataUseCase = get(),
            getFirebaseFlowerUseCase = get(),
            addAllFlowersInDataUseCase = get()
        )
    }

    viewModel<MainActivityViewModel> {
        MainActivityViewModel(
            getCategoryFlowersUseCase = get(),
        )
    }

    viewModel<AboutFlowerActivityViewModel> {
        AboutFlowerActivityViewModel(
            changeFlowerInDataUseCase = get(),
            getBasketUseCase = get(),
            postAmountValueNullUseCase = get()
        )
    }

    viewModel<BasketFragmentViewModel> {
        BasketFragmentViewModel(
            changeFlowerInDataUseCase = get(),
            postAmountValueNullUseCase = get(),
            getSumOfBasketUseCase = get(),
            getAllFlowersFromDataUseCase = get()
        )
    }
    viewModel<DataAboutBuyerViewModel> {
        DataAboutBuyerViewModel(

        )
    }

    viewModel<BonusFragmentViewModel> {
        BonusFragmentViewModel()
    }

    viewModel<DeliveryFragmentViewModel> {
        DeliveryFragmentViewModel()
    }

    viewModel<TopFragmentsViewModel> {
        TopFragmentsViewModel(
            postAmountValueNullUseCase = get(),
            changeFlowerInDataUseCase = get()
        )
    }

    viewModel<SplashScreenViewModel> {
        SplashScreenViewModel()
    }
}